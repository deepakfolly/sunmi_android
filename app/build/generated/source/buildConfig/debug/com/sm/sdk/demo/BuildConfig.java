/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.sm.sdk.demo;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "com.sm.sdk.demo";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 133;
  public static final String VERSION_NAME = "v2.2.65";
}
